package com.example.springpractice

import com.example.springpractice.models.Product
import org.springframework.data.repository.CrudRepository

// 데이터베이스에 접근하는 영역. DAO(Data Access Object)
// 얘가 직접 DB와 통신하면서 데이터를 CRUD하게 된다.
// 왜 인터페이스인지는 잘 모르겠다. 아마 jpa가 자동으로 구현해서 사용하나보다.
// 모델과 ID의 타입을 제네릭에 설정한다.
interface ProductRepository: CrudRepository<Product, Long> {
//    fun findAll(): List<Product> // Product 들을 반환하는 함수의 형태를 설정해놓는다.

    // CrudRepository를 상속받으면 기본적인 CRUD 함수는 제공되기 때문에 따로 구현하지 않고 넘어간다.
}