package com.example.springpractice

import com.example.springpractice.models.Category
import com.example.springpractice.models.Product

// 각 영역간의 데이터 교환을 위한 객체라고 한다.
// Data Transfer Object

// SELECT 조건에 들어갈 수 있는 속성들을 담고 있는 듯 하다.
data class ReadProductDTO (
    val id: Long? = null,
    val name: String,
    val category: Category
)

// INSERT 할 때 기본적으로 필요한 속성들을 담고 있는 듯 하다.
data class CreateProductDTO (
    val name: String,
    val category: Category
) {
    fun toEntity(): Product = Product(name = name, category = category)
}

// Update 할 때 들어갈 수 있는 속성들
data class UpdateProductDTO (
    val name: String,
    val category: Category
)